/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package models;

import dal.*;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import org.apache.catalina.mapper.Mapper;

/**
 *
 * @author DELL
 * @param <T>
 */
public class DAO<T> extends DBContext {

    private T t;
    private String result = "";

    public DAO() {
    }

    public DAO(T t) {
        this.t = t;
    }

    public String getResult() {
        return result;
    }
 

    public String getAllData(String sql) {
        try {
            PreparedStatement ps = connection.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            ResultSetMetaData rsmd = rs.getMetaData();
            
            while (rs.next()) {
                int i = 1;
                while (i <= rsmd.getColumnCount()) {
                    result += rs.getObject(i) + " ";
                    i++;
                }
                result += "\n";
            }
        } catch (SQLException e) {
        }
        return result;
    }

    public static void main(String[] args) {
        DAO<ArrayList<Account>> acc = new DAO<>();
        System.out.println(acc.getAllData("select * from Employees"));
    }
}
