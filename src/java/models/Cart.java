/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package models;

import dal.Product;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author DELL
 */
public class Cart {

    private List<Product> products;

    public Cart() {
        products = new ArrayList<>();
    }

    public Cart(List<Product> products) {
        this.products = products;
    }

    public List<Product> getProducts() {
        return products;
    }

    public void setProducts(List<Product> products) {
        this.products = products;
    }

    private Product getProductByID(int ID) {
        for (Product p : products) {
            if (p.getProductID() == ID) {
                return p;
            }
        }
        return null;
    }

    public void addProduct(Product p) {
        if (getProductByID(p.getProductID()) == null) {
            products.add(p);
        }
    }

    public void addQuantityProduct(Product p) {
        getProductByID(p.getProductID()).setQuantity(getProductByID(p.getProductID()).getQuantity() + 1);
    }

    public void minusProduct(Product p) {
        if (getProductByID(p.getProductID()).getQuantity() == 1) {
            removeProduct(p.getProductID());
        } else {
            getProductByID(p.getProductID()).setQuantity(getProductByID(p.getProductID()).getQuantity() - 1);
        }
    }

    public void removeProduct(int ID) {
        if (getProductByID(ID) != null) {
            products.remove(getProductByID(ID));
        }
    }
}
