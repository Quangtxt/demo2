/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package controllers;

import dal.*;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import models.*;

/**
 *
 * @author DELL
 */
public class OrderController extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        if (req.getParameter("action") != null) {
            new OrderDAO().cancelOrder(req.getParameter("ID"));
        }

        OrderDAO ord = new OrderDAO(new OrderDAO().getAllOrder());
        ord.setRange(10);
        ord.setPage(req.getParameter("page"));
        req.setAttribute("page", ord.getPage());
        req.setAttribute("endP", ord.getEndPage());
        req.setAttribute("listOrd", ord.getOrdersPani());
        req.setAttribute("listCus", new CustomerDAO().getALlCustomer());
        req.setAttribute("listEmp", new EmployeesDAO().getAllEmployee());
        req.getRequestDispatcher("/order.jsp").forward(req, resp);
    }

}
